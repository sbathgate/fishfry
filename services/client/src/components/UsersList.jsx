import React from "react";
import PropTypes from "prop-types";
import UpdateUser from "./UpdateUser";

const UsersList = props => {
  return (
    <div>
      <table className="table is-hoverable is-fullwidth">
        <thead>
          <tr>
            <th>ID</th>
            <th>Email</th>
            <th>Username</th>
            <th>First Name</th>
            <th>Last Name</th>
            {props.isAuthenticated() && <th />}
          </tr>
        </thead>
        <tbody>
          {props.users.map(user => {
            return (
              <tr key={user.id}>
                <td>{user.id}</td>
                <td>{user.email}</td>
                <td className="username">{user.username}</td>
                <td className="first_name">{user.first_name}</td>
                <td className="last_name">{user.last_name}</td>
                {props.isAuthenticated() && (
                  <td>
                    <button
                      className="button is-warning is-small"
                      onClick={() => props.updateUser(user.id)}
                    >
                      Update
                    </button>
                  </td>
                )}
                {props.isAuthenticated() && (
                  <td>
                    <button
                      className="button is-danger is-small"
                      onClick={() => props.removeUser(user.id)}
                    >
                      Delete
                    </button>
                  </td>
                )}
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};

UsersList.propTypes = {
  users: PropTypes.array.isRequired,
  isAuthenticated: PropTypes.func.isRequired
};

export default UsersList;
