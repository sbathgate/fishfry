import React from "react";
import PropTypes from "prop-types";

const LocationsList = props => {
  return (
    <div>
      <table className="table is-hoverable is-fullwidth">
        <thead>
          <tr>
            <th>ID</th>
            <th>Location Name</th>
            {props.isAuthenticated() && <th />}
          </tr>
        </thead>
        <tbody>
          {props.locations.map(location => {
            return (
              <tr key={location.id}>
                <td>{location.id}</td>
                <td className="location_name">{location.location_name}</td>
                {props.isAuthenticated() && (
                  <td>
                    <button
                      className="button is-danger is-small"
                      onClick={() => props.removeLocation(location.id)}
                    >
                      Delete
                    </button>
                  </td>
                )}
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};

LocationsList.propTypes = {
  locations: PropTypes.array.isRequired,
  removeLocation: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.func.isRequired
};

export default LocationsList;
