import React from "react";
import PropTypes from "prop-types";
import { Formik } from "formik";
import * as Yup from "yup";
import "./form.css";

const AddLocation = props => (
  <Formik
    initialValues={{
      location_name: ""
    }}
    onSubmit={(values, { setSubmitting, resetForm }) => {
      props.addLocation(values);
      resetForm();
      setSubmitting(false);
    }}
    validationSchema={Yup.object().shape({
      location_name: Yup.string()
        .required("Location name is required.")
        .min(4, "Location name must be greater than 3 characters.")
    })}
  >
    {props => {
      const {
        values,
        touched,
        errors,
        isSubmitting,
        handleChange,
        handleBlur,
        handleSubmit
      } = props;
      return (
        <form onSubmit={handleSubmit}>
          <div className="field">
            <label className="label" htmlFor="input-location_name">
              Location name
            </label>
            <input
              name="location_name"
              id="input-location_name"
              className={
                errors.location_name && touched.location_name
                  ? "input error"
                  : "input"
              }
              type="text"
              placeholder="Enter a location name"
              value={values.location_name}
              onChange={handleChange}
              onBlur={handleBlur}
            />
            {errors.location_name && touched.location_name && (
              <div className="input-feedback">{errors.location_name}</div>
            )}
          </div>
          <input
            type="submit"
            className="button is-primary"
            value="Submit"
            disabled={isSubmitting}
          />
        </form>
      );
    }}
  </Formik>
);

AddLocation.propTypes = {
  addLocation: PropTypes.func.isRequired
};

export default AddLocation;
