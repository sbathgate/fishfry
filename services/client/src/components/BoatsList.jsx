import React from "react";
import PropTypes from "prop-types";
import UpdateBoat from "./UpdateBoat";

const BoatsList = props => {
  return (
    <div>
      <table className="table is-hoverable is-fullwidth">
        <thead>
          <tr>
            <th>ID</th>
            <th>Boat Name</th>
            <th>Status</th>
            <th>Captain</th>
            {props.isAuthenticated() && <th />}
          </tr>
        </thead>
        <tbody>
          {props.boats.map(boat => {
            return (
              <tr key={boat.id}>
                <td>{boat.id}</td>
                <td className="boat_name">{boat.boat_name}</td>
                <td className="location">{boat.location}</td>
                <td className="captain">{boat.captain}</td>
                {props.isAuthenticated() && (
                  <td>
                    <button
                      className="button is-warning is-small"
                      onClick={() => props.updateBoats(boat.id)}
                    >
                      Update
                    </button>
                  </td>
                )}
                {props.isAuthenticated() && (
                  <td>
                    <button
                      className="button is-danger is-small"
                      onClick={() => props.removeBoat(boat.id)}
                    >
                      Delete
                    </button>
                  </td>
                )}
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};

BoatsList.propTypes = {
  boats: PropTypes.array.isRequired,
  removeBoat: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.func.isRequired
};

export default BoatsList;
