import React from "react";
import PropTypes from "prop-types";
import { Formik } from "formik";
import * as Yup from "yup";
import "./form.css";

const AddBoat = props => (
  <Formik
    initialValues={{
      boat_name: "",
      location: "",
      captain: ""
    }}
    onSubmit={(values, { setSubmitting, resetForm }) => {
      props.addBoat(values);
      resetForm();
      setSubmitting(false);
    }}
    validationSchema={Yup.object().shape({
      boat_name: Yup.string()
        .required("Boat name is required.")
        .min(6, "Boat name must be greater than 5 characters."),
      location: Yup.string()
        .required("First name is required.")
        .min(2, "First name must be greater than 1 characters."),
      captain: Yup.string()
        .required("Last name is required.")
        .min(2, "Last name must be greater than 1 characters.")
    })}
  >
    {props => {
      const {
        values,
        touched,
        errors,
        isSubmitting,
        handleChange,
        handleBlur,
        handleSubmit
      } = props;
      return (
        <form onSubmit={handleSubmit}>
          <div className="field">
            <label className="label" htmlFor="input-boat_name">
              Boat name
            </label>
            <input
              name="boat_name"
              id="input-boat_name"
              className={
                errors.boat_name && touched.boat_name ? "input error" : "input"
              }
              type="text"
              placeholder="Enter a boat name"
              value={values.boat_name}
              onChange={handleChange}
              onBlur={handleBlur}
            />
            {errors.boat_name && touched.boat_name && (
              <div className="input-feedback">{errors.boat_name}</div>
            )}
          </div>
          <div className="field">
            <label className="label" htmlFor="input-location">
              Boat location
            </label>
            <input
              name="location"
              id="input-location"
              className={
                errors.location && touched.location ? "input error" : "input"
              }
              type="text"
              placeholder="Enter a location"
              value={values.location}
              onChange={handleChange}
              onBlur={handleBlur}
            />
            {errors.location && touched.location && (
              <div className="input-feedback">{errors.location}</div>
            )}
          </div>
          <div className="field">
            <label className="label" htmlFor="input-captain">
              Captain
            </label>
            <input
              name="captain"
              id="input-captain"
              className={
                errors.captain && touched.captain ? "input error" : "input"
              }
              type="text"
              placeholder="Enter a captain"
              value={values.captain}
              onChange={handleChange}
              onBlur={handleBlur}
            />
            {errors.captain && touched.captain && (
              <div className="input-feedback">{errors.captain}</div>
            )}
          </div>

          <input
            type="submit"
            className="button is-primary"
            value="Submit"
            disabled={isSubmitting}
          />
        </form>
      );
    }}
  </Formik>
);

AddBoat.propTypes = {
  addBoat: PropTypes.func.isRequired
};

export default AddBoat;
