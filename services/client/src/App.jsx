import React, { Component } from "react";
import axios from "axios";
import { Route, Switch } from "react-router-dom";
import Modal from "react-modal";

import UsersList from "./components/UsersList";
import BoatsList from "./components/BoatsList";
import LocationsList from "./components/LocationsList";
import About from "./components/About";
import NavBar from "./components/NavBar";
import LoginForm from "./components/LoginForm";
import RegisterForm from "./components/RegisterForm";
import UserStatus from "./components/UserStatus";
import Message from "./components/Message";
import AddUser from "./components/AddUser";
import AddBoat from "./components/AddBoat";
import AddLocation from "./components/AddLocation";

const modalStyles = {
  content: {
    top: "0",
    left: "0",
    right: "0",
    bottom: "0",
    border: 0,
    background: "transparent"
  }
};

Modal.setAppElement(document.getElementById("root"));

class App extends Component {
  constructor() {
    super();
    this.state = {
      users: [],
      boats: [],
      locations: [],
      title: "Fishfry Tours",
      accessToken: null,
      messageType: null,
      messageText: null,
      showModal: false
    };
  }
  componentDidMount() {
    this.getUsers();
    this.getBoats();
    this.getLocations();
  }
  getUsers() {
    axios
      .get(`${process.env.REACT_APP_BACKEND_SERVICE_URL}/users`)
      .then(res => {
        this.setState({ users: res.data });
      })
      .catch(err => {
        console.log(err);
      });
  }
  addUser = data => {
    axios
      .post(`${process.env.REACT_APP_BACKEND_SERVICE_URL}/users`, data)
      .then(res => {
        this.getUsers();
        this.setState({
          username: "",
          email: "",
          first_name: "",
          last_name: ""
        });
        this.handleCloseModal();
        this.createMessage("success", "User added.");
      })
      .catch(err => {
        console.log(err);
        this.handleCloseModal();
        this.createMessage("danger", "That user already exists.");
      });
  };
  updateUser = data => {
    axios
      .put(`${process.env.REACT_APP_BACKEND_SERVICE_URL}/users/:id`, data)
      .then(res => {
        this.getUsers();
        this.setState({
          username: "",
          email: "",
          first_name: "",
          last_name: ""
        });
        this.handleCloseModal();
        this.createMessage("success", "User added.");
      })
      .catch(err => {
        console.log(err);
        this.handleCloseModal();
        this.createMessage("danger", "That user already exists.");
      });
  };
  getBoats() {
    axios
      .get(`${process.env.REACT_APP_BACKEND_SERVICE_URL}/boats`)
      .then(res => {
        this.setState({ boats: res.data });
      })
      .catch(err => {
        console.log(err);
      });
  }
  addBoat = data => {
    axios
      .post(`${process.env.REACT_APP_BACKEND_SERVICE_URL}/boats`, data)
      .then(res => {
        this.getBoats();
        this.setState({ boat_name: "", location: "", captain: "" });
        this.handleCloseModal();
        this.createMessage("success", "Boat added.");
      })
      .catch(err => {
        console.log(err);
        this.handleCloseModal();
        this.createMessage("danger", "That boat already exists.");
      });
  };
  getLocations() {
    axios
      .get(`${process.env.REACT_APP_BACKEND_SERVICE_URL}/locations`)
      .then(res => {
        this.setState({ locations: res.data });
      })
      .catch(err => {
        console.log(err);
      });
  }
  addLocation = data => {
    axios
      .post(`${process.env.REACT_APP_BACKEND_SERVICE_URL}/locations`, data)
      .then(res => {
        this.getLocations();
        this.setState({
          location_name: ""
        });
        this.handleCloseModal();
        this.createMessage("success", "Location added.");
      })
      .catch(err => {
        console.log(err);
        this.handleCloseModal();
        this.createMessage("danger", "That location already exists.");
      });
  };
  handleRegisterFormSubmit = data => {
    const url = `${process.env.REACT_APP_BACKEND_SERVICE_URL}/auth/register`;
    axios
      .post(url, data)
      .then(res => {
        console.log(res.data);
        this.createMessage("success", "You have registered successfully.");
      })
      .catch(err => {
        console.log(err);
        this.createMessage("danger", "That user already exists.");
      });
  };
  handleLoginFormSubmit = data => {
    const url = `${process.env.REACT_APP_BACKEND_SERVICE_URL}/auth/login`;
    axios
      .post(url, data)
      .then(res => {
        this.setState({ accessToken: res.data.access_token });
        this.getUsers();
        window.localStorage.setItem("refreshToken", res.data.refresh_token);
        this.createMessage("success", "You have logged in successfully.");
      })
      .catch(err => {
        console.log(err);
        this.createMessage("danger", "Incorrect email and/or password.");
      });
  };
  isAuthenticated = () => {
    if (this.state.accessToken || this.validRefresh()) {
      return true;
    }
    return false;
  };
  validRefresh() {
    const token = window.localStorage.getItem("refreshToken");
    if (token) {
      axios
        .post(`${process.env.REACT_APP_BACKEND_SERVICE_URL}/auth/refresh`, {
          refresh_token: token
        })
        .then(res => {
          this.setState({ accessToken: res.data.access_token });
          this.getUsers();
          window.localStorage.setItem("refreshToken", res.data.refresh_token);
          return true;
        })
        .catch(err => {
          return false;
        });
    }
    return false;
  }
  logoutUser = () => {
    window.localStorage.removeItem("refreshToken");
    this.setState({ accessToken: null });
    this.createMessage("success", "You have logged out.");
  };
  createMessage = (type, text) => {
    this.setState({
      messageType: type,
      messageText: text
    });
    setTimeout(() => {
      this.removeMessage();
    }, 3000);
  };
  removeMessage = () => {
    this.setState({
      messageType: null,
      messageText: null
    });
  };
  handleOpenModal = () => {
    this.setState({ showModal: true });
  };
  handleCloseModal = () => {
    this.setState({ showModal: false });
  };
  removeUser = user_id => {
    axios
      .delete(`${process.env.REACT_APP_BACKEND_SERVICE_URL}/users/${user_id}`)
      .then(res => {
        this.getUsers();
        this.createMessage("success", "User removed.");
      })
      .catch(err => {
        console.log(err);
        this.createMessage("danger", "Something went wrong.");
      });
  };
  removeBoat = boat_id => {
    axios
      .delete(`${process.env.REACT_APP_BACKEND_SERVICE_URL}/boats/${boat_id}`)
      .then(res => {
        this.getBoats();
        this.createMessage("success", "Boat removed.");
      })
      .catch(err => {
        console.log(err);
        this.createMessage("danger", "Something went wrong.");
      });
  };
  removeLocation = location_id => {
    axios
      .delete(
        `${process.env.REACT_APP_BACKEND_SERVICE_URL}/locations/${location_id}`
      )
      .then(res => {
        this.getLocations();
        this.createMessage("success", "Location removed.");
      })
      .catch(err => {
        console.log(err);
        this.createMessage("danger", "Something went wrong.");
      });
  };
  render() {
    return (
      <div>
        <NavBar
          title={this.state.title}
          logoutUser={this.logoutUser}
          isAuthenticated={this.isAuthenticated}
        />
        <section className="section">
          <div className="container">
            {this.state.messageType && this.state.messageText && (
              <Message
                messageType={this.state.messageType}
                messageText={this.state.messageText}
                removeMessage={this.removeMessage}
              />
            )}
            <div className="columns">
              <div className="column is-half">
                <br />
                <Switch>
                  <Route
                    exact
                    path="/"
                    render={() => (
                      <div>
                        <h1 className="title is-1">Boats</h1>
                        <hr />
                        <br />
                        {this.isAuthenticated() && (
                          <button
                            onClick={this.handleOpenModal}
                            className="button is-primary"
                          >
                            Add Boat
                          </button>
                        )}
                        <br />
                        <br />
                        <Modal
                          isOpen={this.state.showModal}
                          style={modalStyles}
                        >
                          <div className="modal is-active">
                            <div className="modal-background" />
                            <div className="modal-card">
                              <header className="modal-card-head">
                                <p className="modal-card-title">Add Boat</p>
                                <button
                                  className="delete"
                                  aria-label="close"
                                  onClick={this.handleCloseModal}
                                />
                              </header>
                              <section className="modal-card-body">
                                <AddBoat addBoat={this.addBoat} />
                              </section>
                            </div>
                          </div>
                        </Modal>
                        <BoatsList
                          boats={this.state.boats}
                          removeBoat={this.removeBoat}
                          isAuthenticated={this.isAuthenticated}
                        />
                      </div>
                    )}
                  />
                  <Route
                    exact
                    path="/users"
                    render={() => (
                      <div>
                        <h1 className="title is-1">Users</h1>
                        <hr />
                        <br />
                        {this.isAuthenticated() && (
                          <button
                            onClick={this.handleOpenModal}
                            className="button is-primary"
                          >
                            Add User
                          </button>
                        )}
                        <br />
                        <br />
                        <Modal
                          isOpen={this.state.showModal}
                          style={modalStyles}
                        >
                          <div className="modal is-active">
                            <div className="modal-background" />
                            <div className="modal-card">
                              <header className="modal-card-head">
                                <p className="modal-card-title">Add User</p>
                                <button
                                  className="delete"
                                  aria-label="close"
                                  onClick={this.handleCloseModal}
                                />
                              </header>
                              <section className="modal-card-body">
                                <AddUser addUser={this.addUser} />
                              </section>
                            </div>
                          </div>
                        </Modal>
                        <UsersList
                          users={this.state.users}
                          removeUser={this.removeUser}
                          updateUser={this.updateUser}
                          isAuthenticated={this.isAuthenticated}
                        />
                      </div>
                    )}
                  />
                  <Route
                    exact
                    path="/locations"
                    render={() => (
                      <div>
                        <h1 className="title is-1">Locations</h1>
                        <hr />
                        <br />
                        {this.isAuthenticated() && (
                          <button
                            onClick={this.handleOpenModal}
                            className="button is-primary"
                          >
                            Add Location
                          </button>
                        )}
                        <br />
                        <br />
                        <Modal
                          isOpen={this.state.showModal}
                          style={modalStyles}
                        >
                          <div className="modal is-active">
                            <div className="modal-background" />
                            <div className="modal-card">
                              <header className="modal-card-head">
                                <p className="modal-card-title">Add Location</p>
                                <button
                                  className="delete"
                                  aria-label="close"
                                  onClick={this.handleCloseModal}
                                />
                              </header>
                              <section className="modal-card-body">
                                <AddLocation addLocation={this.addLocation} />
                              </section>
                            </div>
                          </div>
                        </Modal>
                        <LocationsList
                          locations={this.state.locations}
                          removeLocation={this.removeLocation}
                          isAuthenticated={this.isAuthenticated}
                        />
                      </div>
                    )}
                  />
                  <Route exact path="/about" component={About} />
                  <Route
                    exact
                    path="/status"
                    render={() => (
                      <UserStatus
                        accessToken={this.state.accessToken}
                        isAuthenticated={this.isAuthenticated}
                      />
                    )}
                  />
                  <Route
                    exact
                    path="/register"
                    render={() => (
                      <RegisterForm
                        // eslint-disable-next-line react/jsx-handler-names
                        handleRegisterFormSubmit={this.handleRegisterFormSubmit}
                        isAuthenticated={this.isAuthenticated}
                      />
                    )}
                  />
                  <Route
                    exact
                    path="/login"
                    render={() => (
                      <LoginForm
                        // eslint-disable-next-line react/jsx-handler-names
                        handleLoginFormSubmit={this.handleLoginFormSubmit}
                        isAuthenticated={this.isAuthenticated}
                      />
                    )}
                  />
                </Switch>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default App;
