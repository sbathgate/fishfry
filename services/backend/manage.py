# manage.py


import sys

from flask.cli import FlaskGroup

from project import create_app
from project import db
from project.api.users.models import User
from project.api.boats.models import Boat
from project.api.locations.models import Location


app = create_app()
cli = FlaskGroup(create_app=create_app)


@cli.command('recreate_db')
def recreate_db():
    db.drop_all()
    db.create_all()
    db.session.commit()

@cli.command('seed_db')
def seed_db():
    db.session.add(User(username='bob_ross', first_name="Bob", last_name="Ross", email="bob@fishfrytours.ca", password="supersecret"))
    db.session.add(User(username='marie_drabick', first_name="Marie", last_name="Drabick", email="marie@fishfrytours.ca", password="supersecret"))
    db.session.add(User(username='jake_thomson', first_name="Jake", last_name="Thomson", email="jake@fishfrytours.ca", password="supersecret"))
    db.session.add(User(username='michael_herman', first_name="Michael", last_name="Herman", email="michael@fishfrytours.ca", password="supersecret"))
    db.session.add(User(username='jenna_clarke', first_name="Jenna", last_name="Clarke", email="jenna@fishfrytours.ca", password="supersecret"))
    db.session.add(User(username='coral_szpotowicz', first_name="Coral", last_name="Szpotowicz", email="coral@fishfrytours.ca", password="supersecret"))
    db.session.add(User(username='andrew_timlock', first_name="Andrew", last_name="Timlock", email="andrew@fishfrytours.ca", password="supersecret"))
    db.session.add(User(username='cody_carr', first_name="Cody", last_name="Carr", email="cody@fishfrytours.ca", password="supersecret"))
    db.session.add(User(username='lisa_gable', first_name="Lisa", last_name="Gable", email="lisa@fishfrytours.ca", password="supersecret"))
    db.session.add(User(username='shaun_henderson', first_name="Shaun", last_name="Henderson", email="shaun@fishfrytours.ca", password="supersecret"))
    db.session.add(User(username='jordan_cholak', first_name="Jordan", last_name="Cholak", email="jordan@fishfrytours.ca", password="supersecret"))
    db.session.add(User(username='chelsea_turner', first_name="Chelsea", last_name="Turner", email="chelsea@fishfrytours.ca", password="supersecret"))
    db.session.add(Boat(boat_name="Tufted Puffin"))
    db.session.add(Boat(boat_name="Gannet"))
    db.session.add(Boat(boat_name="Osprey"))
    db.session.add(Boat(boat_name="Cormorant"))
    db.session.add(Boat(boat_name="Kingfisher"))
    db.session.add(Boat(boat_name="Heron"))
    db.session.add(Boat(boat_name="Pelican"))
    db.session.add(Boat(boat_name="Loon"))
    db.session.add(Location(location_name="Docked"))
    db.session.add(Location(location_name="Out to Sea"))
    db.session.add(Location(location_name="Inbound to Harbour"))
    db.session.add(Location(location_name="Maintenance"))
    db.session.commit()


if __name__ == '__main__':
    cli()