# project/api/locations/admin.py


from flask_admin.contrib.sqla import ModelView


class LocationsAdminView(ModelView):
    column_searchable_list = ("location_name",)
    column_editable_list = (
        "location_name",
        "created_date",
    )
    column_filters = ("location_name",)
    column_sortable_list = (
        "location_name",
        "active",
        "created_date",
    )
    column_default_sort = ("created_date", True)
