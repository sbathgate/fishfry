# project/api/models.py


import os


from sqlalchemy.sql import func

from project import db


class Location(db.Model):

    __tablename__ = "locations"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    location_name = db.Column(db.String(128), nullable=False)
    boat_id = db.Column(db.Integer, db.ForeignKey("boats.id"))
    active = db.Column(db.Boolean(), default=True, nullable=False)
    created_date = db.Column(db.DateTime, default=func.now(), nullable=False)

    def __init__(self, location_name=""):
        self.location_name = location_name

    def __repr__(self):
        return f"{self.location_name}"


if os.getenv("FLASK_ENV") == "development":
    from project import admin
    from project.api.locations.admin import LocationsAdminView

    admin.add_view(LocationsAdminView(Location, db.session))
