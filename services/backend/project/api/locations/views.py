# project/api/locations.py


from flask import request
from flask_restx import Resource, fields, Namespace

from project.api.locations.crud import (
    add_location,
    delete_location,
    get_all_locations,
    get_location_by_location_name,
    get_location_by_id,
    update_location,
)

locations_namespace = Namespace("locations")

location = locations_namespace.model(
    "Location",
    {
        "id": fields.Integer(readOnly=True),
        "location_name": fields.String(required=True),
        "created_date": fields.DateTime,
    },
)


class LocationsList(Resource):
    @locations_namespace.marshal_with(location, as_list=True)
    def get(self):
        return get_all_locations(), 200

    @locations_namespace.expect(location, validate=True)
    @locations_namespace.response(201, "<location_location> was added!")
    @locations_namespace.response(400, "Sorry. That location already exists.")
    def post(self):
        post_data = request.get_json()
        location_name = post_data.get("location_name")
        location = post_data.get("location")
        response_object = {}

        location = get_location_by_location_name(location_name)
        if location:
            response_object["message"] = "Sorry. That location already exists."
            return response_object, 400
        add_location(location_name)
        response_object["message"] = f"{location} was added!"
        return response_object, 201


class Locations(Resource):
    @locations_namespace.marshal_with(location)
    @locations_namespace.response(200, "Success")
    @locations_namespace.response(404, "Location <location_id> does not exist")
    def get(self, location_id):
        location = get_location_by_id(location_id)
        if not location:
            locations_namespace.abort(404, f"Location {location_id} does not exist")
        return location, 200

    @locations_namespace.expect(location, validate=True)
    @locations_namespace.response(200, "<location_is> was updated!")
    @locations_namespace.response(404, "Location <location_id> does not exist")
    def put(self, location_id):
        post_data = request.get_json()
        location_name = post_data.get("location_name")
        location = post_data.get("location")
        response_object = {}

        location = get_location_by_id(location_id)
        if not location:
            locations_namespace.abort(404, f"Location {location_id} does not exist")
        update_location(location, location_name)
        response_object["message"] = f"{location.id} was updated!"
        return response_object, 200

    @locations_namespace.response(200, "<location_is> was removed!")
    @locations_namespace.response(404, "Location <location_id> does not exist")
    def delete(self, location_id):
        response_object = {}
        location = get_location_by_id(location_id)
        if not location:
            locations_namespace.abort(404, f"Location {location_id} does not exist")
        delete_location(location)
        response_object["message"] = f"{location.location} was removed!"
        return response_object, 200


locations_namespace.add_resource(LocationsList, "")
locations_namespace.add_resource(Locations, "/<int:location_id>")
