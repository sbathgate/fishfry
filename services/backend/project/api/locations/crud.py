# project/api/crud.py


from project import db
from project.api.locations.models import Location


def get_all_locations():
    return Location.query.all()


def get_location_by_id(location_id):
    return Location.query.filter_by(id=location_id).first()


def get_location_by_location_name(location_name):
    return Location.query.filter_by(location_name=location_name).first()


def add_location(location_name):
    location = Location(location_name=location_name)
    db.session.add(location)
    db.session.commit()
    return location


def update_location(location, location_name):
    location.location_name = location_name
    db.session.commit()
    return location


def delete_location(location):
    db.session.delete(location)
    db.session.commit()
    return location
