# project/api/models.py


import os

from sqlalchemy.sql import func

from project import db
from project.api.users.models import User  # noqa: F401
from project.api.locations.models import Location  # noqa: F401


class Boat(db.Model):

    __tablename__ = "boats"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    boat_name = db.Column(db.String(128), nullable=False)
    captain = db.relationship("User", backref="boats", lazy=True, uselist=False)
    location = db.relationship("Location", backref="boats", lazy=True, uselist=False)
    active = db.Column(db.Boolean(), default=True, nullable=False)
    created_date = db.Column(db.DateTime, default=func.now(), nullable=False)

    def __init__(self, boat_name=""):
        self.boat_name = boat_name


if os.getenv("FLASK_ENV") == "development":
    from project import admin
    from project.api.boats.admin import BoatsAdminView

    admin.add_view(BoatsAdminView(Boat, db.session))
