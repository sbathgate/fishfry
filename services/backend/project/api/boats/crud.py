# project/api/crud.py


from project import db
from project.api.boats.models import Boat


def get_all_boats():
    return Boat.query.all()


def get_boat_by_id(boat_id):
    return Boat.query.filter_by(id=boat_id).first()


def get_boat_by_boat_name(boat_name):
    return Boat.query.filter_by(boat_name=boat_name).first()


def add_boat(boat_name):
    boat = Boat(boat_name=boat_name)
    db.session.add(boat)
    db.session.commit()
    return boat


def update_boat(boat, boat_name, captain, location):
    boat.boat_name = boat_name
    boat.captain = captain
    boat.location = location
    db.session.commit()
    return boat


def delete_boat(boat):
    db.session.delete(boat)
    db.session.commit()
    return boat
