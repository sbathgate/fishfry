# project/api/boats.py


from flask import request
from flask_restx import Resource, fields, Namespace

from project.api.boats.crud import (
    add_boat,
    delete_boat,
    get_all_boats,
    get_boat_by_boat_name,
    get_boat_by_id,
    update_boat,
)

boats_namespace = Namespace("boats")

boat = boats_namespace.model(
    "Boat",
    {
        "id": fields.Integer(readOnly=True),
        "boat_name": fields.String(required=True),
        "created_date": fields.DateTime,
        "captain": fields.String(),
        "location": fields.String(),
    },
)


class BoatsList(Resource):
    @boats_namespace.marshal_with(boat, as_list=True)
    def get(self):
        return get_all_boats(), 200

    @boats_namespace.expect(boat, validate=True)
    @boats_namespace.response(201, "<boat_name> was added!")
    @boats_namespace.response(400, "Sorry. That boat name already exists.")
    def post(self):
        post_data = request.get_json()
        boat_name = post_data.get("boat_name")
        response_object = {}

        boat = get_boat_by_boat_name(boat_name)
        if boat:
            response_object["message"] = "Sorry. That boat name already exists."
            return response_object, 400
        add_boat(boat_name)
        response_object["message"] = f"{boat_name} was added!"
        return response_object, 201


class Boats(Resource):
    @boats_namespace.marshal_with(boat)
    @boats_namespace.response(200, "Success")
    @boats_namespace.response(404, "Boat <boat_id> does not exist")
    def get(self, boat_id):
        boat = get_boat_by_id(boat_id)
        if not boat:
            boats_namespace.abort(404, f"Boat {boat_id} does not exist")
        return boat, 200

    @boats_namespace.expect(boat, validate=True)
    @boats_namespace.response(200, "<boat_is> was updated!")
    @boats_namespace.response(404, "Boat <boat_id> does not exist")
    def put(self, boat_id):
        post_data = request.get_json()
        boat_name = post_data.get("boat_name")
        captain = post_data.get("captain")
        location = post_data.get("location")
        response_object = {}

        boat = get_boat_by_id(boat_id)
        if not boat:
            boats_namespace.abort(404, f"Boat {boat_id} does not exist")
        update_boat(boat, boat_name, captain, location)
        response_object["message"] = f"{boat.id} was updated!"
        return response_object, 200

    @boats_namespace.response(200, "<boat_is> was removed!")
    @boats_namespace.response(404, "Boat <boat_id> does not exist")
    def delete(self, boat_id):
        response_object = {}
        boat = get_boat_by_id(boat_id)
        if not boat:
            boats_namespace.abort(404, f"Boat {boat_id} does not exist")
        delete_boat(boat)
        response_object["message"] = f"{boat.boat_name} was removed!"
        return response_object, 200


boats_namespace.add_resource(BoatsList, "")
boats_namespace.add_resource(Boats, "/<int:boat_id>")
