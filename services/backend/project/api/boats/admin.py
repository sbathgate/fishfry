# project/api/boats/admin.py


from flask_admin.contrib.sqla import ModelView


class BoatsAdminView(ModelView):
    column_searchable_list = ("boat_name",)
    column_editable_list = (
        "boat_name",
        "created_date",
    )
    column_filters = ("boat_name",)
    column_sortable_list = (
        "boat_name",
        "active",
        "created_date",
    )
    column_default_sort = ("created_date", True)
