# Fishfry Tours
> Source code for Fishfry Tours Boat Management System. This application enables Fishfry Tours to monitor boat location and corresponding boat captain. 

[![pipeline status](https://gitlab.com/sbathgate/fishfry-tours/badges/master/pipeline.svg)](https://gitlab.com/sbathgate/fishfry-tours/commits/master)

## Development setup

First, install the system dependencies:
* [docker](https://docs.docker.com/)
* [docker-compose](https://docs.docker.com/compose/)
* [git](https://git-scm.com/)
* [make](https://www.gnu.org/software/make/)

Second, download the source code
```sh
git clone https://gitlab.com/sbathgate/fishfry-tours.git
cd fastapi-tdd-docker/
```

Third, set the `REACT_APP_USERS_SERVICE_URL` environment variable then build the project image. 
```sh
export REACT_APP_USERS_SERVICE_URL=http://localhost:5001
make build
```

Fourth, run the containers, create and seed database:
```sh
make start
```

## Tools and Technologies
### Back-end
* Python
* Flask
* Postgres
* Pytest
* Flask-RESTX
* Flask-SQLAlchemy
* Flask-CORS
* Flask-Bcrypt
* PyJWT
* Gunicorn
* Coverage.py
* flake8
* Black
* isort
* Swagger/OpenAPI

### Front-end
* JavaScript
* Node
* React
* Formik
* Jest
* React Testing Library
* Axios
* React Router
* ESLint
* Prettier
* Formik
* Yup

### Tools
* Create React App
* Docker
* Nginx
* HTTPie

### Services
* GitLab
* Heroku

## File Structure
### Within the download you'll find the following directories and files:
```
├── .gitignore
├── .gitlab-ci.yml
├── Dockerfile.deploy
├── README.md
├── docker-compose.yml
├── makefile
├── release.sh
└── services
    ├── client
    │   ├── .dockerignore
    │   ├── .eslintrc.json
    │   ├── .gitignore
    │   ├── Dockerfile
    │   ├── Dockerfile.ci
    │   ├── README.md
    │   ├── coverage
    │   ├── package-lock.json
    │   ├── package.json
    │   ├── public
    │   │   ├── favicon.ico
    │   │   ├── index.html
    │   │   ├── logo192.png
    │   │   ├── logo512.png
    │   │   ├── manifest.json
    │   │   └── robots.txt
    │   └── src
    │       ├── App.jsx
    │       ├── components
    │       │   ├── About.jsx
    │       │   ├── AddUser.jsx
    │       │   ├── LoginForm.jsx
    │       │   ├── Message.jsx
    │       │   ├── NavBar.css
    │       │   ├── NavBar.jsx
    │       │   ├── RegisterForm.jsx
    │       │   ├── UserStatus.jsx
    │       │   ├── UsersList.jsx
    │       │   ├── __tests__
    │       │   │   ├── About.test.jsx
    │       │   │   ├── AddBoat.test.jsx
    │       │   │   ├── AddLocation.test.jsx
    │       │   │   ├── AddUser.test.jsx
    │       │   │   ├── App.test.jsx
    │       │   │   ├── LoginForm.test.jsx
    │       │   │   ├── Message.test.jsx
    │       │   │   ├── NavBar.test.jsx
    │       │   │   ├── RegisterForm.test.jsx
    │       │   │   ├── UserStatus.test.jsx
    │       │   │   ├── BoatsList.test.jsx
    │       │   │   ├── LocationsList.test.jsx
    │       │   │   ├── UsersList.test.jsx
    │       │   │   └── __snapshots__
    │       │   │       ├── About.test.jsx.snap
    │       │   │       ├── AddUser.test.jsx.snap
    │       │   │       ├── App.test.jsx.snap
    │       │   │       ├── LoginForm.test.jsx.snap
    │       │   │       ├── Message.test.jsx.snap
    │       │   │       ├── NavBar.test.jsx.snap
    │       │   │       ├── RegisterForm.test.jsx.snap
    │       │   │       ├── UserStatus.test.jsx.snap
    │       │   │       └── UsersList.test.jsx.snap
    │       │   └── form.css
    │       ├── index.js
    │       └── setupTests.js
    ├── nginx
    │   └── default.conf
    └── users
        ├── .coverage
        ├── .coveragerc
        ├── .dockerignore
        ├── Dockerfile
        ├── Dockerfile.prod
        ├── entrypoint.sh
        ├── htmlcov
        ├── manage.py
        ├── project
        │   ├── __init__.py
        │   ├── api
        │   │   ├── __init__.py
        │   │   ├── auth.py
        │   │   ├── ping.py
        │   │   └── boats
        │   │       ├── __init__.py
        │   │       ├── admin.py
        │   │       ├── crud.py
        │   │       ├── models.py
        │   │       └── views.py
        │   │   └── locations
        │   │       ├── __init__.py
        │   │       ├── admin.py
        │   │       ├── crud.py
        │   │       ├── models.py
        │   │       └── views.py
        │   │   └── users
        │   │       ├── __init__.py
        │   │       ├── admin.py
        │   │       ├── crud.py
        │   │       ├── models.py
        │   │       └── views.py
        │   ├── config.py
        │   ├── db
        │   │   ├── Dockerfile
        │   │   └── create.sql
        │   └── tests
        │       ├── __init__.py
        │       ├── conftest.py
        │       ├── pytest.ini
        │       ├── test_admin.py
        │       ├── test_auth.py
        │       ├── test_config.py
        │       ├── test_ping.py
        │       ├── test_user_model.py
        │       ├── test_users.py
        │       └── test_users_unit.py
        ├── requirements-dev.txt
        ├── requirements.txt
        └── setup.cfg
```

## Common Commands
### Docker Compose
Set the `REACT_APP_USERS_SERVICE_URL` environment variable:
```sh
export REACT_APP_USERS_SERVICE_URL=http://localhost:5001
```

Build the images:
```sh
docker-compose build
```

Build and spin up the new containers:
```sh
docker-compose up -d --build
```

To stop the containers:
```sh
docker-compose stop
```

To bring down the containers:
```sh
docker-compose down
```

### Client
Run the tests without coverage:
```sh
docker-compose exec client npm test
```

Run the tests with coverage:
```sh
docker-compose exec client npm test --coverage
```

Check formatting with Prettier:
```sh
docker-compose exec client npm run prettier:check
```

Lint with eslint:
```sh
docker-compose exec client npm run lint
```

### Server
Create the database:
```sh
docker-compose exec users python manage.py recreate_db
```

Seed the database:
```sh
docker-compose exec users python manage.py seed_db
```

Run the tests without coverage:
```sh
docker-compose exec users python -m pytest "project/tests" -p no:warnings
```

Run the tests with coverage:
```sh
docker-compose exec users python -m pytest "project/tests" -p no:warnings --cov="project"
```

Lint with Flake8:
```sh
docker-compose exec users flake8 project
```

Run Black and isort with check options:
```sh
docker-compose exec users black project --check
docker-compose exec users /bin/sh -c "isort project/**/*.py --check-only"
```

Make code changes with Black and isort:
```sh
docker-compose exec users black project
docker-compose exec users /bin/sh -c "isort project/**/*.py"
```

### Postgres
Want to access the database via psql?
```sh
docker-compose exec users-db psql -U postgres
```

Then, you can connect to the database and run SQL queries. For example:
```sh
\c users_dev
select * from users;
```

### Other Commands
Want to force a build?
```sh
docker-compose build --no-cache
```

Remove images:
```sh
docker rmi $(docker images -q)
```

## Release History

* 0.1.0
    * Initial release

## Acknowledgements
This project wouldn't have been possible without the excellent [Test Driven Development: Authentication with Flask, React and Docker Course](https://testdriven.io/courses/auth-flask-react/) developed by [Michael Herman](https://mherman.org/) on [testdriven.io](https://testdriven.io). 

## Contributing

1. Fork it (<https://github.com/sbathgate/article-summarizer/fork>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request
